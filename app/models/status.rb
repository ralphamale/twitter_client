require './lib/twitter_session.rb'
require 'open-uri'

class Status < ActiveRecord::Base
  attr_accessible :body, :twitter_status_id, :twitter_user_id
  #attr_accessor :twitter_user_id
  validates :body, :twitter_status_id, :twitter_user_id, presence: true

  belongs_to :user,
  foreign_key: :twitter_user_id,
  primary_key: :twitter_status_id


  def self.fetch_by_twitter_user_id!(twitter_user_id)
    parsed_tweets = TwitterSession.get(
      "statuses/user_timeline",
      { :user_id => twitter_user_id })

   #   debugger

      parse_json(parsed_tweets)
      puts "Fetched done"

    self.where(:twitter_user_id => twitter_user_id)
  end

  def self.get_by_twitter_user_id(user_id)
    if self.internet_connection?
      fetch_by_twitter_user_id!(user_id)
    end

    self.where(:twitter_user_id => user_id)

  end

  def self.internet_connection?
    begin
      true if open("http://www.google.com/")
    rescue
      false
    end
  end

  def self.post(body)
    parsed_post = TwitterSession.post("statuses/update", { :status => body })

    params = {body: body, twitter_status_id: parsed_post["id_str"], twitter_user_id: parsed_post["user"]["id_str"]}

    Status.new(params).save!
  end

  def self.parse_json(statuses)
    #
   # debugger

    twit_user_id = statuses.first["user"]["id_str"]
    old_ids = Status.where(twitter_user_id: statuses.first["user"]["id_str"]).pluck(:twitter_status_id)


    statuses.each do |status|

      next if old_ids.include?(status["id_str"])

      new_status = Status.new
        #   debugger if status["user"]["id_str"].nil?
      new_status.body = status["text"]
      new_status.twitter_status_id = status["id_str"]
      new_status.twitter_user_id = twit_user_id
      debugger if status["user"]["id_str"].nil?
      new_status.save!
      puts "Saved?"
    end



  end

    #
  # t.string   "body",              :null => false
  # t.string   "twitter_status_id", :null => false
  # t.string   "twitter_user_id",   :null => false
  # t.datetime "created_at",        :null => false
  # t.datetime "updated_at",        :null => false
end
