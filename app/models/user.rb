require './lib/twitter_session.rb'

class User < ActiveRecord::Base
  attr_accessible :screen_name, :twitter_user_id
  validates :screen_name, :twitter_user_id, uniqueness: true
  validates :screen_name, :twitter_user_id, presence: true

  has_many :statuses,
  class_name: "Status",
  foreign_key: :twitter_user_id,
  primary_key: :twitter_status_id

  def fetch_statuses!
    Status.fetch_by_twitter_user_id!(self.twitter_user_id)
  end

  def self.fetch_by_screen_name!(screen_name)
    twitter_user = TwitterSession.get(
          "users/show",
          { :screen_name => screen_name })

    parse_twitter_user(twitter_user)

  end

  def self.get_by_screen_name(screen_name)
    fetched = self.where(:screen_name => screen_name)

    if fetched.empty?
      fetched = fetch_by_screen_name!(screen_name)
    end

      fetched

  end

  def self.parse_twitter_user(twitter_user) #helper
    user_id = twitter_user["id_str"]
    screeny_name = twitter_user["screen_name"]

    created_usr = User.new({twitter_user_id: user_id, screen_name: screeny_name})
    created_usr.save!

    created_usr
  end
end
