require 'launchy'
require 'oauth'
require 'addressable/uri'
require 'debugger'
require 'yaml'
require 'json'
require_relative 'access_keys'

class TwitterSession
  CONSUMER_KEY = Keys::CONSUMER_KEY
  CONSUMER_SECRET = Keys::CONSUMER_SECRET
  CONSUMER = OAuth::Consumer.new(
    CONSUMER_KEY, CONSUMER_SECRET, :site => "https://twitter.com")

  TOKEN_FILE = "access_token.yml"

  def self.access_token
    if File.exist?(TOKEN_FILE)
      # reload token from file
      File.open(TOKEN_FILE) { |f| YAML.load(f) }
    else
      # copy the old code that requested the access token into a
      # `request_access_token` method.
      access_token = request_access_token
      File.open(TOKEN_FILE, "w") { |f| YAML.dump(access_token, f) }

      access_token
    end
  end

  def self.request_access_token

    request_token = CONSUMER.get_request_token
    authorize_url = request_token.authorize_url

    puts "Go to this URL: #{authorize_url}"
    Launchy.open(authorize_url)

    puts "Login, and type your verification code in"
    oauth_verifier = gets.chomp
    @access_token = request_token.get_access_token(
      :oauth_verifier => oauth_verifier
    )
    @access_token
  end

  def self.path_to_url(path, query_values = nil)
    address = Addressable::URI.new(
      :scheme => "https",
      :host => "api.twitter.com",
      :path => "1.1/#{path}.json",
      :query_values => query_values
    ).to_s

    address
  end

    def self.get(path, query_values)

      get_unparsed = self.access_token.get(path_to_url(path, query_values)).body
      _get = JSON.parse(get_unparsed)
      _get
    end

    def self.post(path, req_params)
      post_unparsed = self.access_token.post(path_to_url(path, req_params)).body
      _post = JSON.parse(post_unparsed)
      debugger
      _post
    end
end#
#
# puts TwitterSession.get(
#   "statuses/user_timeline",
#   { :user_id => "35206553" }
# )
# puts TwitterSession.post(
#   "statuses/update",
#   { :status => "New Status!" }
# )